﻿using System;
using System.Collections.Generic;
using CommandLine;

namespace FileWatcher
{
    public class Options
    {
        [Value(0, Required = true, MetaName = "path", HelpText = "The file or directory path you want to watch.")]
        public string Path { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args)
            .WithParsed<Options>(o =>
            {
                Console.WriteLine($"watching {o.Path}");
                Watcher.Watch(o.Path);
            })
            .WithNotParsed(HandleParseError);
        }

        static void HandleParseError(IEnumerable<Error> errs) {
            foreach(var e in errs) {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
