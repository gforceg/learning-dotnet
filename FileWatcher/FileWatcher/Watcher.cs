
using System;
using System.IO;

namespace FileWatcher
{
    public static class Watcher {
        public static void Watch(string filePath) {

            (string, string) fp = ParsePathAndFilter(filePath);

            Console.WriteLine($"Path: {fp.Item1}");
            Console.WriteLine($"Filter: {fp.Item2}");

            using(var fw = new FileSystemWatcher{
                Path = fp.Item1,
                Filter = fp.Item2,
                IncludeSubdirectories = false,
                EnableRaisingEvents = true
            }) {
                fw.Created += RenameOnChange;

                while (Console.Read() != 'q') ;
            }
        }

        private static void RenameOnChange(object sender, FileSystemEventArgs eventArgs) {
            if (eventArgs.ChangeType.ToString() == "Created") {
                Console.WriteLine($"file Created {eventArgs.FullPath}");
                try {
                    Console.WriteLine($"renaming file {eventArgs.Name}");

                    var dir = Path.GetDirectoryName(eventArgs.FullPath);
                    var newFileName = GetTimeStampString();
                    var newFilePath = $"{dir}/{newFileName}";

                    File.Move(eventArgs.FullPath, newFilePath);
                } catch(Exception ex) {
                    Console.WriteLine(ex.Message);
                }
            }
            // Console.WriteLine($"{eventArgs.ChangeType.ToString()} {eventArgs.Name} {eventArgs}");
        }

        private static string GetTimeStampString() {
            var now = DateTime.Now;
            var date = now.ToShortDateString().ToString();
            var time = now.ToShortTimeString().ToString();
            var sec = now.Second.ToString();
            var ms = now.Millisecond.ToString();
            var name =  $"{date}_{time}_{sec}_{ms}.txt"
                .Replace(" ", "")
                .Replace(":", "_")
                .Replace("/", "_");
            return name;
        }

        public static (string, string) ParsePathAndFilter(string filePath) {
            int cutoff = filePath.LastIndexOf('/');

            var path = filePath.Substring(0, cutoff);
            var filter = filePath.Substring(cutoff + 1);
            
            return (path, filter);
        }
    }
}