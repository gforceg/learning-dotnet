using System;
using Xunit;
using FileWatcher;
using System.IO;

namespace FileWatcher.test
{
    public class WatcherTests
    {
        public string twd { get { return $"{Directory.GetCurrentDirectory()}/test-files"; } }

        [Fact]
        public void Parses_Path_And_Filter()
        {
            Assert.Equal(
                Watcher.ParsePathAndFilter($"{twd}/*"),
                ($"{twd}", "*")
            );
        }

        [Fact]
        public void Parses_Path()
        {
            Assert.Equal(
                Watcher.ParsePathAndFilter($"{twd}"),
                ($"{twd}", "*")
            );
        }
    }
}
